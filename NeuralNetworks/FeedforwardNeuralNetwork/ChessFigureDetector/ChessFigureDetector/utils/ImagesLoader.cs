﻿using ChessFigureDetector.model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ChessFigureDetector.utils
{
    class ImagesLoader
    {
        public static List<ChessFigure> findImagesInDirectory(string sourceDirectory)
        {
            List<ChessFigure> figures = new List<ChessFigure>();

            foreach (string fileName in Directory.GetFiles(sourceDirectory, "*.png"))
            {
                figures.Add(new ChessFigure()
                {
                    FileName = fileName,
                    Type = getFigureTypeByFileName(fileName)
                });
            }

            return figures;
        }

        public static FigureType getFigureTypeByFileName(string fileName)
        {
            string name = Path.GetFileNameWithoutExtension(fileName);
            if (name.StartsWith("regina")) return FigureType.Regina;
            if (name.StartsWith("turn")) return FigureType.Turn;
            if (name.StartsWith("nebun")) return FigureType.Nebun;
            if (name.StartsWith("cal")) return FigureType.Cal;
            if (name.StartsWith("rege")) return FigureType.Rege;

            return FigureType.Unknown;
        }

        public static double[] loadImageAsDoubleArray(string fileName)
        {
            double[] result = null;

            using (var bmp = new Bitmap(Image.FromFile(fileName)))
            {
                var data = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
                    ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);
                int size = data.Stride * data.Height;
                byte[] bytes = new byte[size];
                Marshal.Copy(data.Scan0, bytes, 0, size);
                bmp.UnlockBits(data);
                data = null;
                result = new double[size];
                for (int i = 0; i < size; i++)
                {
                    result[i] = (double)bytes[i] / (double) byte.MaxValue; 
                }

                bytes = null;
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();

            return result;
        }
    }
}

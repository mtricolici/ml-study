﻿using Encog.ML.Data.Basic;
using Encog.ML.SVM;
using Encog.ML.SVM.Training;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example1.svm
{
    public class MySVM
    {
        private const int MAX_ITERATIONS = 1000;
        private SupportVectorMachine svm;
        private double trainError;

        public MySVM(KernelType kernel, double trainError)
        {
            this.svm = new SupportVectorMachine(2, SVMType.SupportVectorClassification, kernel);
            this.trainError = trainError;
        }

        public bool train(double[][] input, double[][] ideal)
        {
            var trainingSet = new BasicMLDataSet(input, ideal);
            
            //var train = new SVMTrain(this.svm, trainingSet);
            var train = new SVMSearchTrain(this.svm, trainingSet);

            int it = 1;
            do
            {
                train.Iteration();
                it++;
            } while ((train.Error > this.trainError) && (it < MAX_ITERATIONS));

            return train.Error <= this.trainError;
        }

        public double Compute(double[] input)
        {
            var res = this.svm.Compute(new BasicMLData(input));
            return res[0];
        }
    }
}

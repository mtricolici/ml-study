﻿using OrangeCaptcha.network;
using OrangeCaptcha.neural_network;
using OrangeCaptcha.processing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrangeCaptcha
{
    public partial class MainForm : Form
    {
        private const string SAVE_NETWORK_LOCATION = @"c:\!fast-storage\orange.nn";

        private OrangeImageLoader remoteImageLoader = new OrangeImageLoader();
        private MyNeuralNetwork network = null;

        public MainForm()
        {
            InitializeComponent();
            pictureBoxSample.SizeMode = PictureBoxSizeMode.AutoSize;
        }

        private async void butLoadRemoteImage_Click(object sender, EventArgs e)
        {
            butLoadRemoteImage.Enabled = false;
            pictureBoxSample.Image = null;
            pictureBoxWithoutNoise.Image = null;
            probabilityGridView1.Clear();

            if ((network != null) && (network.IsTrained))
            {
                labCaptchaText.Text = "Detecting ...";
            }

            imagesList1.Clear();

            pictureBoxSample.Image = await remoteImageLoader.getIt();
            pictureBoxWithoutNoise.Image = await new NoiseReduction2().applyFilter(pictureBoxSample.Image);
            imagesList1.LoadImages(await new ImageSplitter().split(pictureBoxWithoutNoise.Image));
            if ((network != null) && (network.IsTrained))
            {
                var captcha = await network.GetCaptcha(imagesList1.Images);
                labCaptchaText.Text = captcha.Captcha;
                probabilityGridView1.SetData(ref captcha.Probability);
            }

            butLoadRemoteImage.Enabled = true;
        }

        private void imagesList1_OnImageSave(Image img)
        {
            if (img == null) return;
            var res = saveFileDialog1.ShowDialog();
            if (res != System.Windows.Forms.DialogResult.OK) return;
            img.Save(saveFileDialog1.FileName);
        }

        private void butSelectFolder_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = tbTrainingDir.Text;
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tbTrainingDir.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void trainingLog(string message)
        {
            tbTrainingLog.AppendText(message + "\r\n");
        }

        private async void butTrainNetwork_Click(object sender, EventArgs e)
        {
            butTrainNetwork.Enabled = false;
            butSelectFolder.Enabled = false;
            tbTrainingDir.Enabled = false;
            tbTrainingLog.Clear();

            this.network = new MyNeuralNetwork(SAVE_NETWORK_LOCATION);
            if (!this.network.IsTrained)
            {

                var progressIndicator = new Progress<string>(trainingLog);

                if (!await this.network.Train(tbTrainingDir.Text, progressIndicator))
                {
                    MessageBox.Show("Training Failed! :(");
                }
                else
                {
                    MessageBox.Show("Training Success ;)");
                }
            }
            else
            {
                MessageBox.Show("Network is already trained. enjoy!");
            }

            butSelectFolder.Enabled = true;
            tbTrainingDir.Enabled = true;
            butTrainNetwork.Enabled = true;
        }

        private void butSaveNetwork_Click(object sender, EventArgs e)
        {
            if ((network == null) || (!network.IsTrained))
            {
                MessageBox.Show("Train it first!");
            }
            else
            {
                network.saveNetwork();
            }
        }

        private void pictureBoxWithoutNoise_Click(object sender, EventArgs e)
        {
            if (pictureBoxWithoutNoise.Image == null) return;

            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                pictureBoxWithoutNoise.Image.Save(saveFileDialog1.FileName);
            }
        }
    }
}

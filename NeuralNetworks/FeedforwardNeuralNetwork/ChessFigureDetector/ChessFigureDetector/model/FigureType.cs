﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessFigureDetector.model
{
    enum FigureType
    {
        Unknown,
        Turn,
        Regina,
        Nebun,
        Cal,
        Rege
    }
}

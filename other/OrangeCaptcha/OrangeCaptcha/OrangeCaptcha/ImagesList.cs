﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OrangeCaptcha.neural_network;

namespace OrangeCaptcha
{
    public delegate void  ImagesListOnSaveDelegate(Image img);

    public partial class ImagesList : UserControl
    {
        private const int IMAGES_PER_ROW = 5;
        private const int deltaX = 20;
        private const int deltaY = 20;

        private int x;
        private int y;
        private int currentRowControlsCount;

        private ImagesListOnSaveDelegate handlerOnImagesListSave;

        public event ImagesListOnSaveDelegate OnImageSave
        {
            add { handlerOnImagesListSave += value; }
            remove { handlerOnImagesListSave -= value; }
        }

        public List<Bitmap> Images { get; private set; }

        public ImagesList()
        {
            InitializeComponent();
            this.Images = new List<Bitmap>();
        }

        public void Clear()
        {
            this.Controls.Clear();
            this.x = 10;
            this.y = 10;
            this.currentRowControlsCount = 0;
            this.Images.Clear();
        }

        public void LoadImages(List<Image> images)
        {
            this.Clear();

            if ((images == null) || (images.Count == 0)) return;

            for (int i = 0; i < images.Count; i++)
            {
                PictureBox pb = new PictureBox();
                pb.SizeMode = PictureBoxSizeMode.AutoSize;
                pb.Left = x;
                pb.Top = y;
                pb.Image = images[i];
                pb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                pb.Click += new System.EventHandler(onPictureBoxClick);

                Controls.Add(pb);
                addImageForNeuralNetwork(images[i]);

                currentRowControlsCount++;
                
                if (currentRowControlsCount == IMAGES_PER_ROW)
                {
                    currentRowControlsCount = 0;
                    x = 10;
                    y += images[i].Height + deltaY;
                }
                else
                {
                    x += images[i].Width + deltaX;
                }
            }
        }

        private void addImageForNeuralNetwork(Image image)
        {
            Bitmap bmp = new Bitmap(image);
            this.Images.Add(bmp);
        }

        void onPictureBoxClick(object sender, EventArgs e)
        {
            if (handlerOnImagesListSave != null)
            {
                handlerOnImagesListSave(((PictureBox)sender).Image);
            }
        }
    }
}

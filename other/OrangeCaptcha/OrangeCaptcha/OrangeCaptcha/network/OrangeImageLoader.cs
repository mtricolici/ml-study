﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace OrangeCaptcha.network
{
    public class OrangeImageLoader
    {
        private const string USER_AGENT = "Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en; rv:1.8.1.2pre) Gecko/20070223";

        private static Regex ImageCaptcha = new Regex(
            "src=.(CaptchaImage.axd.guid=\\S+)\"",
            RegexOptions.Compiled | RegexOptions.Multiline);

        public OrangeImageLoader()
        {
        }

        public async Task<Image> getIt()
        {
            Image img = null;

            string captchaUrl;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Referer", "http://www.orange.md/");                
                client.DefaultRequestHeaders.Add("User-Agent", USER_AGENT);

                using (var response = await client.GetAsync("https://www.orangetext.md/"))
                {
                    using (HttpContent content = response.Content)
                    {
                        string htmlPage = await content.ReadAsStringAsync();
                        Match m = ImageCaptcha.Match(htmlPage);
                        if (m == null || !m.Success)
                        {
                            return null;
                        }

                        captchaUrl = m.Groups[1].Value;
                    }
                }

                captchaUrl = string.Format("https://www.orangetext.md/{0}", captchaUrl);
                using (var response = await client.GetAsync(captchaUrl))
                {
                    using (HttpContent content = response.Content)
                    {
                        byte[] data = await content.ReadAsByteArrayAsync();
                        using (var ms  = new MemoryStream(data))
                        {
                            img = Image.FromStream(ms);
                        }

                        data = null;
                    }
                }
            }

            return img;
        }
    }
}

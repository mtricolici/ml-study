﻿using Encog.ML.SVM;
using Example1.svm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Example1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            cbKernelType.SelectedIndex = 2;
        }

        private void panColor_Click(object sender, EventArgs e)
        {
            colorDialog1.Color = drawPanel.DrawingColor;
            var r = colorDialog1.ShowDialog();
            if (r == System.Windows.Forms.DialogResult.OK)
            {
                drawPanel.DrawingColor = colorDialog1.Color;
                panColor.BackColor = colorDialog1.Color;
            }
        }

        private void butClear_Click(object sender, EventArgs e)
        {
            drawPanel.Clear();
            drawPanel.Invalidate();
        }

        private KernelType getKernelType()
        {
            switch (cbKernelType.SelectedIndex)
            {
                case 0: return KernelType.Linear;
                case 1: return KernelType.Poly;
                case 2: return KernelType.RadialBasisFunction;
                case 3: return KernelType.Sigmoid;
                case 4: return KernelType.Precomputed;
            }

            throw new Exception("oops. internal error1");
        }

        private void butClassify_Click(object sender, EventArgs e)
        {
            if (drawPanel.pixels.Count < 1)
            {
                MessageBox.Show("Add some pixels. Dude!");
                return;
            }
            double maxError = 0;
            try
            {
                maxError = double.Parse(cbError.Text); 
            } catch (Exception)
            {
                MessageBox.Show("Invalid error value");
                return;
            }

            int maxX = drawPanel.ClientRectangle.Width;
            int maxY = drawPanel.ClientRectangle.Height;

            MySVM svm = new MySVM(getKernelType(), maxError);

            var trainingInput = DataTransformation.getSVMTrainingInput(maxX, maxY, drawPanel.pixels);
            var trainingIdeal = DataTransformation.getSVMTrainingIdeal(drawPanel.pixels);

            if (!svm.train(trainingInput, trainingIdeal))
            {
                MessageBox.Show("Training failed :(");
                return;
            }

            double[] testData = new double[2];

            // let's test it!
            for (int x = 2; x < maxX; x += 3)
            {
                var pix = new List<DrawingPanel.DrawingPixel>();

                for (int y = 2; y < maxY; y += 3)
                {
                    testData[0] = x / (double) maxX;
                    testData[1] = y / (double) maxY;

                    double reply = svm.Compute(testData);
                    var p = new DrawingPanel.DrawingPixel();
                    p.point.X = x;
                    p.point.Y = y;
                    p.color = DataTransformation.doubleToColor(reply);
                    pix.Add(p);
                }

                drawPanel.addTestPixels(ref pix);
                drawPanel.Invalidate();
            }
        }
    }
}

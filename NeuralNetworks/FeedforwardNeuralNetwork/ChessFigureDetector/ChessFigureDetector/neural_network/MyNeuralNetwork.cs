﻿using ChessFigureDetector.model;
using ChessFigureDetector.utils;
using Encog.Engine.Network.Activation;
using Encog.ML.Data;
using Encog.ML.Data.Basic;
using Encog.ML.Data.Dynamic;
using Encog.Neural.Networks;
using Encog.Neural.Networks.Layers;
using Encog.Neural.Networks.Training.Propagation.Resilient;
using Encog.Persist;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessFigureDetector.neural_network
{
    class MyNeuralNetwork
    {
        private const int IMAGE_SIZE = 60 * 60;
        private const int HIDDEN_LAYER_NEURONS_COUNT = 70; // hz what value should be here :D
        private const int FIGURES_COUNT = 5;
        private const double COMPUTE_BOTTOM_LIMIT = 0.9;
        private const double TRAINING_MAX_ERROR = 0.0001;

        private BasicNetwork network;
        private string storageFileName;

        public bool IsTrained { get; private set; }

        public MyNeuralNetwork(string fileName)
        {
            this.network = null;
            this.IsTrained = false;
            this.storageFileName = fileName;

            if (!tryLoadNetwork()) {
                createNewNetwork();
            }
        }

        public void Train(ref List<ChessFigure> figures)
        {
            var trainingSet = createTrainingDataSet(ref figures);
            var train = new ResilientPropagation(network, trainingSet);

            int it = 1;
            do
            {
                train.Iteration();
                it++;
                Console.WriteLine("--training it: {0}, Error: {1}", it, train.Error);

            } while (train.Error > TRAINING_MAX_ERROR);
        }

        public FigureType DetectFigureFromImage(string imageFileName)
        {
            FigureType result = FigureType.Unknown;
            var input = new BasicMLData(ImagesLoader.loadImageAsDoubleArray(imageFileName));
            var output = network.Compute(input);
            if (output != null)
            {
                if (output[0] >= COMPUTE_BOTTOM_LIMIT) result = FigureType.Regina;
                if (output[1] >= COMPUTE_BOTTOM_LIMIT) result = FigureType.Turn;
                if (output[2] >= COMPUTE_BOTTOM_LIMIT) result = FigureType.Nebun;
                if (output[3] >= COMPUTE_BOTTOM_LIMIT) result = FigureType.Cal;
                if (output[4] >= COMPUTE_BOTTOM_LIMIT) result = FigureType.Rege;
            }

            input = null;
            output = null;

            GC.Collect();
            GC.WaitForPendingFinalizers();

            return result;
        }

        private IMLDataSet createTrainingDataSet(ref List<ChessFigure> figures)
        {
            double[][] inputData = new double[figures.Count][];
            double[][] idealResults = new double[figures.Count][];

            for (int i = 0; i < figures.Count; i++)
            {
                inputData[i] = ImagesLoader.loadImageAsDoubleArray(figures[i].FileName);
                idealResults[i] = getIdealResultByType(figures[i].Type);
            }

            var dataset = new BasicMLDataSet(inputData, idealResults);
            inputData = null;
            idealResults = null;

            GC.Collect();
            GC.WaitForPendingFinalizers();

            return dataset;
        }

        private double[] getIdealResultByType(FigureType type)
        {
            switch (type)
            {
                case FigureType.Regina:
                    return new double[] { 1, 0, 0, 0, 0 };
                case FigureType.Turn:
                    return new double[] { 0, 1, 0, 0, 0 };
                case FigureType.Nebun:
                    return new double[] { 0, 0, 1, 0, 0 };
                case FigureType.Cal:
                    return new double[] { 0, 0, 0, 1, 0 };
                case FigureType.Rege:
                    return new double[] { 0, 0, 0, 0, 1 };
            }

            throw new Exception("Unkown image type detected");
        }

        #region create/load/save network

        private bool tryLoadNetwork()
        {
            try
            {
                if (File.Exists(storageFileName))
                {
                    this.network = (BasicNetwork)EncogDirectoryPersistence.LoadObject(
                        new FileInfo(storageFileName));
                    if (this.network != null)
                    {
                        this.IsTrained = true;
                    }
                }
            }
            catch (Exception)
            {
            }
            return this.network != null;
        }

        private void createNewNetwork()
        {
            network = new BasicNetwork();
            network.AddLayer(new BasicLayer(null, true, IMAGE_SIZE));
            network.AddLayer(new BasicLayer(new ActivationSigmoid(), true, HIDDEN_LAYER_NEURONS_COUNT));
            network.AddLayer(new BasicLayer(new ActivationSigmoid(), true, FIGURES_COUNT));
            network.Structure.FinalizeStructure();
            network.Reset();
        }

        public void saveNetwork()
        {
            EncogDirectoryPersistence.SaveObject(
                new FileInfo(storageFileName), this.network);
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OrangeCaptcha.neural_network
{
    class ImagesLoader
    {
        public static double[] loadImageAsDoubleArray(ref Bitmap bmp)
        {
            var data = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
                    ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);
            int size = data.Stride * data.Height;
            byte[] bytes = new byte[size];
            Marshal.Copy(data.Scan0, bytes, 0, size);
            bmp.UnlockBits(data);
            data = null;

            double[] result = new double[size];
            for (int i = 0; i < size; i++)
            {
                result[i] = (double)bytes[i] / (double)byte.MaxValue;
            }

            bytes = null;

            GC.Collect();
            GC.WaitForPendingFinalizers();

            return result;
        }

        public static double[] loadImageAsDoubleArray(string fileName)
        {
            double[] result = null;

            var bmp = new Bitmap(Image.FromFile(fileName));
            result = loadImageAsDoubleArray(ref bmp);
            bmp = null;

            GC.Collect();
            GC.WaitForPendingFinalizers();

            return result;
        }
    }
}

﻿namespace OrangeCaptcha
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabs = new System.Windows.Forms.TabControl();
            this.tabGetData = new System.Windows.Forms.TabPage();
            this.labCaptchaText = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBoxWithoutNoise = new System.Windows.Forms.PictureBox();
            this.pictureBoxSample = new System.Windows.Forms.PictureBox();
            this.butLoadRemoteImage = new System.Windows.Forms.Button();
            this.tabNeuralNetwork = new System.Windows.Forms.TabPage();
            this.butSaveNetwork = new System.Windows.Forms.Button();
            this.tbTrainingLog = new System.Windows.Forms.TextBox();
            this.butTrainNetwork = new System.Windows.Forms.Button();
            this.butSelectFolder = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tbTrainingDir = new System.Windows.Forms.TextBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.probabilityGridView1 = new OrangeCaptcha.ProbabilityGridView();
            this.imagesList1 = new OrangeCaptcha.ImagesList();
            this.tabs.SuspendLayout();
            this.tabGetData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWithoutNoise)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample)).BeginInit();
            this.tabNeuralNetwork.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabs
            // 
            this.tabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabs.Controls.Add(this.tabGetData);
            this.tabs.Controls.Add(this.tabNeuralNetwork);
            this.tabs.Location = new System.Drawing.Point(5, 5);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(671, 470);
            this.tabs.TabIndex = 0;
            // 
            // tabGetData
            // 
            this.tabGetData.Controls.Add(this.probabilityGridView1);
            this.tabGetData.Controls.Add(this.labCaptchaText);
            this.tabGetData.Controls.Add(this.label4);
            this.tabGetData.Controls.Add(this.label2);
            this.tabGetData.Controls.Add(this.imagesList1);
            this.tabGetData.Controls.Add(this.label1);
            this.tabGetData.Controls.Add(this.pictureBoxWithoutNoise);
            this.tabGetData.Controls.Add(this.pictureBoxSample);
            this.tabGetData.Controls.Add(this.butLoadRemoteImage);
            this.tabGetData.Location = new System.Drawing.Point(4, 22);
            this.tabGetData.Name = "tabGetData";
            this.tabGetData.Padding = new System.Windows.Forms.Padding(3);
            this.tabGetData.Size = new System.Drawing.Size(663, 444);
            this.tabGetData.TabIndex = 0;
            this.tabGetData.Text = "Get Data";
            this.tabGetData.UseVisualStyleBackColor = true;
            // 
            // labCaptchaText
            // 
            this.labCaptchaText.AutoSize = true;
            this.labCaptchaText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labCaptchaText.ForeColor = System.Drawing.Color.Blue;
            this.labCaptchaText.Location = new System.Drawing.Point(324, 11);
            this.labCaptchaText.Name = "labCaptchaText";
            this.labCaptchaText.Size = new System.Drawing.Size(56, 13);
            this.labCaptchaText.TabIndex = 7;
            this.labCaptchaText.Text = "???????";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(253, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Detected as:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 172);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Splitted images:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Without Noise:";
            // 
            // pictureBoxWithoutNoise
            // 
            this.pictureBoxWithoutNoise.BackColor = System.Drawing.Color.Black;
            this.pictureBoxWithoutNoise.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxWithoutNoise.Location = new System.Drawing.Point(9, 60);
            this.pictureBoxWithoutNoise.Name = "pictureBoxWithoutNoise";
            this.pictureBoxWithoutNoise.Size = new System.Drawing.Size(244, 95);
            this.pictureBoxWithoutNoise.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxWithoutNoise.TabIndex = 2;
            this.pictureBoxWithoutNoise.TabStop = false;
            this.pictureBoxWithoutNoise.Click += new System.EventHandler(this.pictureBoxWithoutNoise_Click);
            // 
            // pictureBoxSample
            // 
            this.pictureBoxSample.Location = new System.Drawing.Point(128, 6);
            this.pictureBoxSample.Name = "pictureBoxSample";
            this.pictureBoxSample.Size = new System.Drawing.Size(111, 23);
            this.pictureBoxSample.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxSample.TabIndex = 1;
            this.pictureBoxSample.TabStop = false;
            // 
            // butLoadRemoteImage
            // 
            this.butLoadRemoteImage.Location = new System.Drawing.Point(6, 6);
            this.butLoadRemoteImage.Name = "butLoadRemoteImage";
            this.butLoadRemoteImage.Size = new System.Drawing.Size(116, 23);
            this.butLoadRemoteImage.TabIndex = 0;
            this.butLoadRemoteImage.Text = "Load an sample =>";
            this.butLoadRemoteImage.UseVisualStyleBackColor = true;
            this.butLoadRemoteImage.Click += new System.EventHandler(this.butLoadRemoteImage_Click);
            // 
            // tabNeuralNetwork
            // 
            this.tabNeuralNetwork.Controls.Add(this.butSaveNetwork);
            this.tabNeuralNetwork.Controls.Add(this.tbTrainingLog);
            this.tabNeuralNetwork.Controls.Add(this.butTrainNetwork);
            this.tabNeuralNetwork.Controls.Add(this.butSelectFolder);
            this.tabNeuralNetwork.Controls.Add(this.label3);
            this.tabNeuralNetwork.Controls.Add(this.tbTrainingDir);
            this.tabNeuralNetwork.Location = new System.Drawing.Point(4, 22);
            this.tabNeuralNetwork.Name = "tabNeuralNetwork";
            this.tabNeuralNetwork.Padding = new System.Windows.Forms.Padding(3);
            this.tabNeuralNetwork.Size = new System.Drawing.Size(663, 444);
            this.tabNeuralNetwork.TabIndex = 1;
            this.tabNeuralNetwork.Text = "Neural Network Training";
            this.tabNeuralNetwork.UseVisualStyleBackColor = true;
            // 
            // butSaveNetwork
            // 
            this.butSaveNetwork.Location = new System.Drawing.Point(181, 63);
            this.butSaveNetwork.Name = "butSaveNetwork";
            this.butSaveNetwork.Size = new System.Drawing.Size(172, 23);
            this.butSaveNetwork.TabIndex = 5;
            this.butSaveNetwork.Text = "Save Network to HDD";
            this.butSaveNetwork.UseVisualStyleBackColor = true;
            this.butSaveNetwork.Click += new System.EventHandler(this.butSaveNetwork_Click);
            // 
            // tbTrainingLog
            // 
            this.tbTrainingLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTrainingLog.Location = new System.Drawing.Point(23, 108);
            this.tbTrainingLog.Multiline = true;
            this.tbTrainingLog.Name = "tbTrainingLog";
            this.tbTrainingLog.ReadOnly = true;
            this.tbTrainingLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbTrainingLog.Size = new System.Drawing.Size(618, 313);
            this.tbTrainingLog.TabIndex = 4;
            // 
            // butTrainNetwork
            // 
            this.butTrainNetwork.Location = new System.Drawing.Point(23, 63);
            this.butTrainNetwork.Name = "butTrainNetwork";
            this.butTrainNetwork.Size = new System.Drawing.Size(152, 23);
            this.butTrainNetwork.TabIndex = 3;
            this.butTrainNetwork.Text = "Train network";
            this.butTrainNetwork.UseVisualStyleBackColor = true;
            this.butTrainNetwork.Click += new System.EventHandler(this.butTrainNetwork_Click);
            // 
            // butSelectFolder
            // 
            this.butSelectFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectFolder.Location = new System.Drawing.Point(546, 35);
            this.butSelectFolder.Name = "butSelectFolder";
            this.butSelectFolder.Size = new System.Drawing.Size(75, 23);
            this.butSelectFolder.TabIndex = 2;
            this.butSelectFolder.Text = "Select";
            this.butSelectFolder.UseVisualStyleBackColor = true;
            this.butSelectFolder.Click += new System.EventHandler(this.butSelectFolder_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Folder with training images:";
            // 
            // tbTrainingDir
            // 
            this.tbTrainingDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTrainingDir.Location = new System.Drawing.Point(23, 37);
            this.tbTrainingDir.Name = "tbTrainingDir";
            this.tbTrainingDir.Size = new System.Drawing.Size(517, 20);
            this.tbTrainingDir.TabIndex = 0;
            this.tbTrainingDir.Text = "d:\\MachineLearningStudy\\other\\OrangeCaptcha\\TrainingImages\\";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "png";
            this.saveFileDialog1.Filter = "PNG Image|*.png";
            // 
            // probabilityGridView1
            // 
            this.probabilityGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.probabilityGridView1.Location = new System.Drawing.Point(256, 30);
            this.probabilityGridView1.Name = "probabilityGridView1";
            this.probabilityGridView1.Size = new System.Drawing.Size(401, 155);
            this.probabilityGridView1.TabIndex = 8;
            // 
            // imagesList1
            // 
            this.imagesList1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.imagesList1.AutoScroll = true;
            this.imagesList1.BackColor = System.Drawing.Color.DarkGray;
            this.imagesList1.Location = new System.Drawing.Point(6, 188);
            this.imagesList1.Name = "imagesList1";
            this.imagesList1.Size = new System.Drawing.Size(650, 253);
            this.imagesList1.TabIndex = 4;
            this.imagesList1.OnImageSave += new OrangeCaptcha.ImagesListOnSaveDelegate(this.imagesList1_OnImageSave);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 477);
            this.Controls.Add(this.tabs);
            this.Name = "MainForm";
            this.Text = "Captcha Detect example";
            this.tabs.ResumeLayout(false);
            this.tabGetData.ResumeLayout(false);
            this.tabGetData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWithoutNoise)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample)).EndInit();
            this.tabNeuralNetwork.ResumeLayout(false);
            this.tabNeuralNetwork.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage tabGetData;
        private System.Windows.Forms.TabPage tabNeuralNetwork;
        private System.Windows.Forms.Button butLoadRemoteImage;
        private System.Windows.Forms.PictureBox pictureBoxSample;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBoxWithoutNoise;
        private ImagesList imagesList1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button butSelectFolder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbTrainingDir;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button butTrainNetwork;
        private System.Windows.Forms.TextBox tbTrainingLog;
        private System.Windows.Forms.Button butSaveNetwork;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labCaptchaText;
        private ProbabilityGridView probabilityGridView1;

    }
}


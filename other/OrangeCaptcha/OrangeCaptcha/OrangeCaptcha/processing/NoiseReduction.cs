﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeCaptcha.processing
{
    public class NoiseReduction
    {
        public async Task<Image> applyFilter(Image img)
        {
            return await Task.Run(() => reduceNoise(img));
        }

        private Bitmap reduceNoise(Image img)
        {
            Bitmap bmp = new Bitmap(img);

            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    if (isBadPixel(bmp.GetPixel(x, y)))
                    {
                        bmp.SetPixel(x, y, Color.White);
                    }
                }
            }

            return bmp;
        }

        private bool isBadPixel(Color cl)
        {
            byte r = cl.R;
            byte g = cl.G;
            byte b = cl.B;

            if (b > 157) return true;

            if ((r == 255) && (g == 142)) return true;
            if ((r == 194) && (g > 180) && (b > 140)) return true;

            if ((r == 147) && (g == 120) && (b == 111)) return true;
            if ((r == 145) && (g == 101) && (b == 76)) return true;
            if ((r == 143) && (g == 96) && (b == 78)) return true;
            if ((r == 182) && (g == 87) && (b == 93)) return true;
            if ((r == 151) && (g == 100) && (b == 81)) return true;
            if ((r == 157) && (g == 98) && (b == 120)) return true;

            if ((r == 159) && (g == 118) && (b == 116)) return true;
            if ((r == 169) && (g == 112) && (b == 92)) return true;

            return false;
        }
    }
}

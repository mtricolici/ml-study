﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Example1
{
    public partial class DrawingPanel : UserControl
    {
        public struct DrawingPixel
        {
            public Point point;
            public Color color;
        }

        public Color DrawingColor { get; set; }

        public List<DrawingPixel> pixels { get; private set; }
        private List<DrawingPixel> testPixels { get; set; }

        // let's cache PEN & BRUSH objects to not create them for each pixel :)
        private Dictionary<Color, Pen> penCache = new Dictionary<Color, Pen>();
        private Dictionary<Color, Brush> brushCache = new Dictionary<Color, Brush>();

        public DrawingPanel()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            this.DrawingColor = Color.Red;
            this.pixels = new List<DrawingPixel>();
            this.testPixels = new List<DrawingPixel>();
        }

        public void Clear()
        {
            this.pixels = new List<DrawingPixel>();
            this.testPixels = new List<DrawingPixel>();
        }

        public void addTestPixels(ref List<DrawingPixel> pixels)
        {
            this.testPixels.AddRange(pixels);
        }

        private Pen getPen(Color color)
        {
            if (penCache.ContainsKey(color))
            {
                return penCache[color];
            }

            Pen p = new Pen(color);
            this.penCache.Add(color, p);
            return p;
        }

        private Brush getBrush(Color color)
        {
            if (brushCache.ContainsKey(color))
            {
                return brushCache[color];
            }

            Brush b = new SolidBrush(color);
            brushCache.Add(color, b);
            return b;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            int w = this.ClientRectangle.Width;
            int h = this.ClientRectangle.Height;

            g.FillRectangle(getBrush(Color.White), 0, 0, w, h);

            for (int i = 0; i < pixels.Count; i++)
            {
                DrawingPixel p = pixels[i];
                g.FillEllipse(getBrush(p.color),
                    p.point.X, p.point.Y, 7, 7);
            }

            for (int i = 0; i < testPixels.Count; i++)
            {
                DrawingPixel p = testPixels[i];
                g.DrawLine(getPen(p.color), p.point.X, p.point.Y,
                    p.point.X + 1, p.point.Y + 1);
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            if (this.testPixels.Count == 0)
            {
                DrawingPixel p = new DrawingPixel();
                p.color = this.DrawingColor;
                p.point.X = e.X;
                p.point.Y = e.Y;
                pixels.Add(p);

                this.Invalidate();
            }
        }
    }
}

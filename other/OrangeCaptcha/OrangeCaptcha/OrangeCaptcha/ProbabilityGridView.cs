﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OrangeCaptcha.neural_network;

namespace OrangeCaptcha
{
    public partial class ProbabilityGridView : UserControl
    {
        public ProbabilityGridView()
        {
            InitializeComponent();
            this.Clear();
        }

        public void Clear()
        {
            grid.Rows.Clear();
            grid.Columns.Clear();
        }

        public void SetData(ref List<CaptchaSymbolResult> data)
        {
            if ((data == null) || (data.Count < 1)) return;

            // Add columns
            for (int i = 0; i < data.Count; i++)
            {
                grid.Columns.Add("col" + i.ToString(), data[i].symbol.ToString());
            }

            // Add rows
            for (int i = 0; i < MyNeuralNetwork.DATA.Length; i++)
            {
                grid.Rows.Add();
            }

            // add Rows values
            for (int col = 0; col < data.Count; col++)
            {
                var sorted = data[col].probability.OrderByDescending(x => x.probability).ToList();

                for (int i = 0; i < sorted.Count; i++)
                {
                    grid[col, i].Value = string.Format(
                        "{0} ({1:0.00}%)", sorted[i].symbol, sorted[i].probability);
                }
            }
        }
    }
}

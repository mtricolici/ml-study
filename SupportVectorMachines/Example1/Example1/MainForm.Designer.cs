﻿namespace Example1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panColor = new System.Windows.Forms.Panel();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.label3 = new System.Windows.Forms.Label();
            this.cbKernelType = new System.Windows.Forms.ComboBox();
            this.butClear = new System.Windows.Forms.Button();
            this.butClassify = new System.Windows.Forms.Button();
            this.drawPanel = new Example1.DrawingPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.cbError = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Click bellow to generate some data:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(562, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Change Color:";
            // 
            // panColor
            // 
            this.panColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panColor.BackColor = System.Drawing.Color.Red;
            this.panColor.Location = new System.Drawing.Point(641, 88);
            this.panColor.Name = "panColor";
            this.panColor.Size = new System.Drawing.Size(43, 26);
            this.panColor.TabIndex = 3;
            this.panColor.Click += new System.EventHandler(this.panColor_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Kernel Type:";
            // 
            // cbKernelType
            // 
            this.cbKernelType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbKernelType.FormattingEnabled = true;
            this.cbKernelType.Items.AddRange(new object[] {
            "Linear",
            "Poly",
            "RadialBasisFunction",
            "Sigmoid",
            "Precomputed"});
            this.cbKernelType.Location = new System.Drawing.Point(96, 12);
            this.cbKernelType.Name = "cbKernelType";
            this.cbKernelType.Size = new System.Drawing.Size(152, 21);
            this.cbKernelType.TabIndex = 5;
            // 
            // butClear
            // 
            this.butClear.Location = new System.Drawing.Point(15, 54);
            this.butClear.Name = "butClear";
            this.butClear.Size = new System.Drawing.Size(103, 33);
            this.butClear.TabIndex = 6;
            this.butClear.Text = "Clear";
            this.butClear.UseVisualStyleBackColor = true;
            this.butClear.Click += new System.EventHandler(this.butClear_Click);
            // 
            // butClassify
            // 
            this.butClassify.Location = new System.Drawing.Point(137, 54);
            this.butClassify.Name = "butClassify";
            this.butClassify.Size = new System.Drawing.Size(131, 33);
            this.butClassify.TabIndex = 7;
            this.butClassify.Text = "Classify Them!";
            this.butClassify.UseVisualStyleBackColor = true;
            this.butClassify.Click += new System.EventHandler(this.butClassify_Click);
            // 
            // drawPanel
            // 
            this.drawPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.drawPanel.BackColor = System.Drawing.Color.White;
            this.drawPanel.DrawingColor = System.Drawing.Color.Red;
            this.drawPanel.Location = new System.Drawing.Point(9, 119);
            this.drawPanel.Name = "drawPanel";
            this.drawPanel.Size = new System.Drawing.Size(679, 353);
            this.drawPanel.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(275, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Max Error:";
            // 
            // cbError
            // 
            this.cbError.FormattingEnabled = true;
            this.cbError.Items.AddRange(new object[] {
            "0.1",
            "0.5",
            "0.01",
            "0.05",
            "0.001"});
            this.cbError.Location = new System.Drawing.Point(336, 13);
            this.cbError.Name = "cbError";
            this.cbError.Size = new System.Drawing.Size(138, 21);
            this.cbError.TabIndex = 9;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 482);
            this.Controls.Add(this.cbError);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.butClassify);
            this.Controls.Add(this.butClear);
            this.Controls.Add(this.cbKernelType);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panColor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.drawPanel);
            this.Name = "MainForm";
            this.Text = "Support Vector Machine example1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DrawingPanel drawPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panColor;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbKernelType;
        private System.Windows.Forms.Button butClear;
        private System.Windows.Forms.Button butClassify;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbError;
    }
}


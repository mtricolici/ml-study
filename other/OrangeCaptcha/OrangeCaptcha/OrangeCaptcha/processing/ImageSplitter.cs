﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeCaptcha.processing
{
    public class ImageSplitter
    {
        private const int MIN_WIDTH = 5;

        public async Task<List<Image>> split(Image filteredImage)
        {
            Bitmap bmp = new Bitmap(filteredImage);
            return await Task.Run(() => splitImage(ref bmp));
        }

        private List<Image> splitImage(ref Bitmap bmp)
        {
            List<Image> images = new List<Image>();

            int w = 0;

            for (int x = 0; x < bmp.Width; x++)
            {
                if(hasInfoPixelsPerVertLine(ref bmp, x)) {
                    w++;
                    continue;
                }

                if (w >= MIN_WIDTH)
                {
                    Image img = cropImage(ref bmp, x - w, x);
                    if (img != null) images.Add(img);
                }

                w = 0;
            }

            if (w >= MIN_WIDTH)
            {
                Image img = cropImage(ref bmp, bmp.Width - w  - 1, bmp.Width - 1);
                if (img != null) images.Add(img);
            }

            return images;
        }

        private bool hasInfoPixelsPerVertLine(ref Bitmap bmp, int x)
        {
            int white = Color.White.ToArgb();

            for (int y = 0; y < bmp.Height; y++)
            {
                if (bmp.GetPixel(x, y).ToArgb() != white)
                {
                    return true;
                }
            }

            return false;
        }

        private bool hasInfoPixelsPerHorizLine(ref Bitmap bmp, int y, int x1, int x2)
        {
            int white = Color.White.ToArgb();

            for (int x = x1; x <= x2; x++ )
            {
                if (bmp.GetPixel(x, y).ToArgb() != white)
                {
                    return true;
                }
            }

            return false;
        }

        private Image cropImage(ref Bitmap srcImage, int x1, int x2)
        {
            int y1 = 0;
            int y2 = srcImage.Height - 1;

            // detect y1
            while (true)
            {
                if (y1 >= srcImage.Height) return null;

                if (hasInfoPixelsPerHorizLine(ref srcImage, y1, x1, x2))
                {
                    if (y1 > 0) y1--;
                    break;
                }
                y1++;
            }

            // detect y2
            while (true)
            {
                //if (y2 - y1 < 32) break; // no need to cut more!
                if (y2 <= y1) return null;
                if (hasInfoPixelsPerHorizLine(ref srcImage, y2, x1, x2))
                {
                    if (y2 < srcImage.Height) y2++;
                    break;
                }
                y2--;
            }

            int dw = x2 - x1 + 1;
            int dh = y2 - y1 + 1;
            
            Rectangle rectSrc = new Rectangle(x1, y1, dw, dh);
            Rectangle rectDest = new Rectangle(0, 0, 32, 32);

            Bitmap bmp = new Bitmap(32, 32);

            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.FillRectangle(new SolidBrush(Color.White), 0, 0, 32, 32);
                g.DrawImage(srcImage, rectDest, rectSrc, GraphicsUnit.Pixel); 
            }

            return bmp;
        }
    }
}

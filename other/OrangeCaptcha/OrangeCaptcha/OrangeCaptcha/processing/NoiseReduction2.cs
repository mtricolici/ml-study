﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeCaptcha.processing
{
    public class NoiseReduction2
    {
        private static int white = Color.White.ToArgb();

        public async Task<Image> applyFilter(Image img)
        {
            return await Task.Run(() => reduceNoise(new Bitmap(img)));
        }

        private Bitmap reduceNoise(Bitmap bmp)
        {
            // step 1 
            //bmp.Save(@"d:\!!!!\001.bmp");
            reduceTooBrightPixels(ref bmp);

            //int st = 2;

            // step 2 clean all 1x1 rectangles!
            do
            {
                //bmp.Save(string.Format("d:/!!!!/{0:000}.bmp", st++));
            } while (reduceRectangle(ref bmp, 1));

            // step 3 clean all 2x2 rectangles
            do
            {
                //bmp.Save(string.Format("d:/!!!!/{0:000}.bmp", st++));
            } while (reduceRectangle(ref bmp, 2));

            // step 4 clean all 3x3 rectangles
            do
            {
                //bmp.Save(string.Format("d:/!!!!/{0:000}.bmp", st++));
            } while (reduceRectangle(ref bmp, 3));

            //bmp.Save(string.Format("d:/!!!!/{0:000}.bmp", st++));

            return cropEmptyArea(ref bmp);
        }

        private void reduceTooBrightPixels(ref Bitmap bmp)
        {
            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    if (bmp.GetPixel(x, y).GetBrightness() > 0.7)
                    {
                        bmp.SetPixel(x, y, Color.White);
                    }
                }
            }
        }

        private bool reduceRectangle(ref Bitmap bmp, int size)
        {
            bool noiseFound = false;
            for (int x = 0; x + size <= bmp.Width; x++)
            {
                for (int y = 0; y + size <= bmp.Height; y++)
                {
                    if (checkRectangle(ref bmp, x, y, size))
                    {
                        noiseFound = true;
                    }
                }
            }

            return noiseFound;
        }

        private bool isAlreadyEmptyRectangle(ref Bitmap bmp, int x, int y, int size)
        {
            for (int xi = x; xi < x + size; xi++)
            {
                for (int yi = y; yi < y + size; yi++)
                {
                    if (bmp.GetPixel(xi, yi).ToArgb() != white) return false;
                }
            }

            return true;
        }

        private bool isEmptyBoxAroundRectangle(ref Bitmap bmp, int x, int y, int size)
        {
            int startX = x - 1;
            int endX = x + size;
            int startY = y - 1;
            int endY = y + size;

            for (int ix = startX; ix <= endX; ix++)
            {
                if ((ix < 0) || (ix >= bmp.Width)) continue;

                for (int iy = startY; iy <= endY; iy++)
                {
                    if ((ix != startX) && (ix != endX) && (iy != startY) && (iy != endY)) continue;
                    if ((iy < 0) || (iy >= bmp.Height)) continue;
                    if (bmp.GetPixel(ix, iy).ToArgb() != white)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private bool checkRectangle(ref Bitmap bmp, int x, int y, int size)
        {
            if (isAlreadyEmptyRectangle(ref bmp, x, y, size)) return false;

            if (isEmptyBoxAroundRectangle(ref bmp, x, y, size))
            {
                for (int ix = x; ix < x + size; ix++)
                {
                    for (int iy = y; iy < y + size; iy++)
                    {
                        bmp.SetPixel(ix, iy, Color.White);
                    }
                }

                return true;
            }

            return false;
        }

        private bool isEmptyRow(ref Bitmap bmp, int y)
        {
            for (int x = 0; x < bmp.Width; x++)
            {
                if (bmp.GetPixel(x, y).ToArgb() != white) return false;
            }
            return true;
        }

        private bool isEmptyCol(ref Bitmap bmp, int x)
        {
            for (int y = 0; y < bmp.Height; y++)
            {
                if (bmp.GetPixel(x, y).ToArgb() != white) return false;
            }
            return true;
        }

        private Bitmap cropEmptyArea(ref Bitmap img)
        {
            int x1 = 0;
            int x2 = img.Width - 1;
            int y1 = 0;
            int y2 = img.Height - 1;

            // find y1
            do
            {
                if (y1 >= img.Height) return null;
                if (!isEmptyRow(ref img, y1)) break;
                y1++;
            } while (true);

            // find y2
            do
            {
                if (y2 <= y1) return null;
                if (!isEmptyRow(ref img, y2)) break;
                y2--;
            } while (true);

            // find x1
            do
            {
                if (x1 >= img.Width) return null;
                if (!isEmptyCol(ref img, x1)) break;
                x1++;
            } while (true);

            // find x2
            do
            {
                if (x2 < x1) return null;
                if (!isEmptyCol(ref img, x2)) break;
                x2--;
            } while (true);

            int dw = x2 - x1;
            int dh = y2 - y1;

            Rectangle rectSrc = new Rectangle(x1, y1, dw, dh);
            Rectangle rectDest = new Rectangle(0, 0, dw, dh);

            Bitmap bmp = new Bitmap(dw, dh);

            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.FillRectangle(new SolidBrush(Color.White), 0, 0, dw, dh);
                g.DrawImage(img, rectDest, rectSrc, GraphicsUnit.Pixel);
            }

            return bmp;
        }
    }
}

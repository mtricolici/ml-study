﻿using Encog.Engine.Network.Activation;
using Encog.ML.Data;
using Encog.ML.Data.Basic;
using Encog.Neural.Networks;
using Encog.Neural.Networks.Layers;
using Encog.Neural.Networks.Training.Propagation.Resilient;
using Encog.Persist;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeCaptcha.neural_network
{
    public class MyNeuralNetwork
    {
        //nu toate literele se folosesc
        public const string DATA = "123456789abcdefghklmnopqrstvwxyz";
        private const int IMAGE_SIZE = 32 * 32;
        private const double TRAINING_MAX_ERROR = 0.001;
        private const int TRAINING_MAX_ITERATIONS = 1000;
        private const double COMPUTE_BOTTOM_LIMIT = 0.01;

        public bool IsTrained { get { return bTrained; } }

        private bool bTrained = false;
        private BasicNetwork network;
        private string storageFileName;

        public MyNeuralNetwork(string fileName)
        {
            this.storageFileName = fileName;
            if (!tryLoadNetwork())
            {
                createNetwork();
            }
            
        }

        public async Task<bool> Train(string trainingDataDir, IProgress<string> progress)
        {
            return await Task.Run(() => trainNetwork(trainingDataDir, progress));
        }

        public async void saveNetwork()
        {
            await Task.Run(() =>
                {
                    EncogDirectoryPersistence.SaveObject(
                        new FileInfo(storageFileName), this.network);
                });
        }

        public async Task<CaptchaResult> GetCaptcha(List<Bitmap> images)
        {
            var result = new CaptchaResult();
            result.Captcha = "";
            result.Probability = new List<CaptchaSymbolResult>();

            for (int i = 0; i < images.Count; i++)
            {
                Bitmap bmp = images[i];
                var symbolResult = await Task.Run(() => ComputeResult(ref bmp));
                bmp = null;

                result.Captcha += symbolResult.symbol;
                result.Probability.Add(symbolResult);

                GC.Collect();
                GC.WaitForPendingFinalizers();
            }

            return result;
        }

        private CaptchaSymbolResult ComputeResult(ref Bitmap img)
        {
            double[] data = ImagesLoader.loadImageAsDoubleArray(ref img);
            var output = network.Compute(new BasicMLData(data));
            data = null;

            var result = new CaptchaSymbolResult();
            result.symbol = '?';
            result.probability = new List<SymbolProbability>();

            if (output != null)
            {
                double max = 0;
                int idx = -1;

                for (int i = 0; i < output.Count; i++)
                {
                    result.probability.Add(new SymbolProbability()
                    {
                        symbol = DATA[i],
                        probability = output[i] * 100
                    });

                    if (output[i] > max)
                    {
                        idx = i;
                        max = output[i];
                    }
                }

                if ((idx >= 0) && (idx < output.Count) && (max >= COMPUTE_BOTTOM_LIMIT))
                {
                    result.symbol = DATA[idx];
                }
            }

            return result;
        }

        private bool tryLoadNetwork()
        {
            try
            {
                if (File.Exists(storageFileName))
                {
                    this.network = (BasicNetwork) EncogDirectoryPersistence.LoadObject(
                        new FileInfo(storageFileName));
                    this.bTrained = true;
                    return true;
                }
            }
            catch (Exception)
            {
            }
            return false;
        }

        private void createNetwork()
        {
            network = new BasicNetwork();
            network.AddLayer(new BasicLayer(null, true, IMAGE_SIZE));
            network.AddLayer(new BasicLayer(new ActivationSigmoid(), true, 500));
            //network.AddLayer(new BasicLayer(new ActivationSigmoid(), true, 100));
            network.AddLayer(new BasicLayer(new ActivationSigmoid(), true, DATA.Length));
            network.Structure.FinalizeStructure();
            network.Reset();
            this.bTrained = false;
        }

        private bool trainNetwork(string dir, IProgress<string> progress)
        {
            if (progress != null) progress.Report("Preparing training...");

            string[] files = Directory.GetFiles(dir, "*.png");
            
            var trainingSet = createTrainingDataSet(ref files);

            var train = new ResilientPropagation(network, trainingSet);

            if (progress != null) progress.Report("Start training...");
            int it = 0;
            do
            {
                train.Iteration();
                it++;
                if (progress != null) {
                    progress.Report(
                        string.Format("Training it #{0}. error:{1}", it, train.Error));
                }

            } while ((it < TRAINING_MAX_ITERATIONS) && (train.Error > TRAINING_MAX_ERROR));

            this.bTrained = train.Error <= TRAINING_MAX_ERROR;
            return bTrained;
        }

        private IMLDataSet createTrainingDataSet(ref string[] files)
        {
            double[][] inputData = new double[files.Length][];
            double[][] idealResults = new double[files.Length][];


            for (int i=0; i<files.Length; i++)
            {
                inputData[i] = ImagesLoader.loadImageAsDoubleArray(files[i]);
                idealResults[i] = getIdealResultByFileName(files[i]);
            }

            var dataset = new BasicMLDataSet(inputData, idealResults);
            inputData = null;
            idealResults = null;

            GC.Collect();
            GC.WaitForPendingFinalizers();

            return dataset;
        }

        private double[] getIdealResultByFileName(string fullFileName)
        {
            double[] result = new double[DATA.Length];
            
            for (int i = 0; i < result.Length; i++) result[i] = 0;

            string shortName = Path.GetFileName(fullFileName);
            int idx = DATA.IndexOf(shortName[0]);
            if (idx >= 0)
            {
                result[idx] = 1.0;
            }

            return result;
        }
    }
}

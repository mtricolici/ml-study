﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example1.svm
{
    public class DataTransformation
    {
        public static double colorToDouble(Color color)
        {
            return color.ToArgb();
        }

        public static Color doubleToColor(double value)
        {
            return Color.FromArgb((int)value);
        }

        public static double[][] getSVMTrainingInput(int maxX, int maxY,
            List<DrawingPanel.DrawingPixel> pixels)
        {
            double[][] input = new double[pixels.Count][];
            for (int i = 0; i < pixels.Count; i++)
            {
                input[i] = new double[2];
                input[i][0] = pixels[i].point.X / (double)maxX;
                input[i][1] = pixels[i].point.Y / (double)maxY;
            }

            return input;
        }

        public static double[][] getSVMTrainingIdeal(List<DrawingPanel.DrawingPixel> pixels)
        {
            double[][] ideal = new double[pixels.Count][];
            for (int i = 0; i < pixels.Count; i++)
            {
                ideal[i] = new double[1];
                ideal[i][0] = DataTransformation.colorToDouble(pixels[i].color);
            }

            return ideal;
        }
    }
}

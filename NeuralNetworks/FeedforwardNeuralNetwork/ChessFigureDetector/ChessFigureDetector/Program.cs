﻿using ChessFigureDetector.model;
using ChessFigureDetector.neural_network;
using ChessFigureDetector.utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChessFigureDetector
{
    static class Program
    {
        private const string IMAGES_DIR = @"d:\MachineLearningStudy\images\ChessFigures\";
        private const string NEURAL_STORAGE = @"c:\!fast-storage\chess-detector.dat";

        static void Main(string[] args)
        {
            log("## Program started");
            try
            {
                log("## Loading images ...");
                List<ChessFigure> figures = ImagesLoader.findImagesInDirectory(IMAGES_DIR);
                log("## Loaded {0} images", figures.Count);

                MyNeuralNetwork nn = new MyNeuralNetwork(NEURAL_STORAGE);
                log("## Loaded neural network.");
                bool isSaveNeeded = false;
                if (nn.IsTrained) log("## network is smart enough. no need to train it ;)");
                else
                {
                    isSaveNeeded = true;
                    log("## network needs training. starting training...");
                    nn.Train(ref figures);
                    log("## Train has finished");
                }

                log("## Let's test the image detection!");

                bool foundFailure = false;
                for (int i = 0; i < figures.Count; i++)
                {
                    FigureType expectedType = ImagesLoader.getFigureTypeByFileName(figures[i].FileName);
                    FigureType type = nn.DetectFigureFromImage(figures[i].FileName);
                    log(" -- file '{0}' detected as '{1}'. expected:{2}",
                        Path.GetFileName(figures[i].FileName), type, expectedType);
                    if ((type == FigureType.Unknown) || (type != expectedType))
                    {
                        foundFailure = true;
                    }
                }

                if (foundFailure)
                {
                    log("# some images were not detected :(");
                }
                else
                {
                    if (isSaveNeeded)
                    {
                        log("# neural network seems to work perfectly! Let's save it to disk ...");
                        nn.saveNetwork();
                        log("#save success");
                    }
                }

                log("## Program finished");
            }
            catch (Exception ex)
            {
                log("## Fatal error: '{0}'", ex.ToString());
            }

            Console.ReadLine();
        }

        static void log(string format, params object[] objs)
        {
            Console.WriteLine(string.Format(format, objs));
        }
    }
}

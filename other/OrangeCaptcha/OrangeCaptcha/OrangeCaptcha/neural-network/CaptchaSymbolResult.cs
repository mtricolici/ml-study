﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeCaptcha.neural_network
{
    public class CaptchaSymbolResult
    {
        public char symbol;
        public List<SymbolProbability> probability;
    }
}
